# Skripsi - Least Square

## Materials

- [Codeigneter 4](https://www.codeigniter.com/)
- [Visual Studio Code](https://code.visualstudio.com/)
- [XAMPP](https://www.apachefriends.org/download.html)
- [GIT](https://git-scm.com/download/win)
- [Bootstrap - 4](https://getbootstrap.com/)

## Installation

- Open htdocs folder in xampp directory
- Nyalakan phpmyadmin dan apache
- Klik kanan di dalam folder htdocs, pilih Git Bash Here
- Ketik `git clone https://gitlab.com/zakyjamaluddin1812/skripsi-least-square.git`

## Maintenance

- Buka Git Bash Here atau terminal di direktori project
- Ketik `git init`
- Ketik `git remote add origin https://gitlab.com/zakyjamaluddin1812/skripsi-least-square.git`

## Updates

- Buka Git Bash Here atau terminal di direktori project
- Ketik `git pull origin main`
- Selesai, seharusnya koding di laptop sudah berubah, menyesuaikan apa yang ada di server
