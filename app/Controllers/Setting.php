<?php

namespace App\Controllers;

class Setting extends BaseController
{
    protected $user;
    public $session;

    public function __construct() {
        $this->user = model('App\Models\User');
        $this->session = session();

        

    }
    public function index()
    {
       
        $data["user"] = $this->user->first();
        return view('setting', $data);
    }
    public function save() {
        $id = $this->user->select("id")->first()["id"];
        $name = $this->request->getPost("name");
        $password = $this->request->getPost("password");
        $passwordBaru = $this->request->getPost("passwordBaru");
        $passwordKonfirmasi = $this->request->getPost("passwordKonfirmasi");
        if($this->user->select("password")->first()["password"] == $password) {
            if($passwordBaru == $passwordKonfirmasi) {
                $user = [
                    "name" => $name,
                    "password" => $password
                ];
                $this->user->update($id, $user);
                return redirect()->to(base_url('/dashboard'));
            } else {
                $this->session->setFlashdata("konfirmasiSalah", true);
                return redirect()->to(base_url('/setting'));

            }
        } else {
            $this->session->setFlashdata("sandiSalah", true);
            return redirect()->to(base_url('/setting'));
        }
    }
}
