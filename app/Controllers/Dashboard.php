<?php

namespace App\Controllers;



class Dashboard extends BaseController
{
    protected $user;
    protected $produk;
    public $session;
    protected $penjualan;
    public function __construct()
    {
        $this->session = session();
        $userModel = model('App\Models\User');
        $this->user = $userModel->first();
        $produk = model('App\Models\Produk');
        $this->produk = $produk->findAll();
        $penjualan = model('App\Models\Penjualan');
        $this->penjualan = $penjualan;
        
    }
    public function index()
    {
        if(!$this->session->has('login')) {
            return redirect()->to(base_url("/"));
        }
        $data["user"] = $this->user;
        $data["produk"] = $this->produk;
        $bulan = date('m');
        $tahun = date('Y');
        if($bulan == 12) {
            $bulanDepan = 1;
            $bulanLalu = $bulan-1;

            $tahunLalu = $tahun;
            $tahunDepan = $tahun+1;
        } elseif($bulan == 1) {
            $bulanDepan = $bulan+1;
            $bulanLalu = 12;

            $tahunLalu = $tahun-1;
            $tahunDepan = $tahun;
        } else {
            $bulanDepan = $bulan+1;
            $bulanLalu = $bulan-1;

            $tahunLalu = $tahun;
            $tahunDepan = $tahun;
        }

        $penjualanMasingMasing = 0;
        foreach($this->penjualan->findAll() as $p) {
            $penjualanMasingMasing += $p['jumlah'];
            
            
        } 
        $data["penjualan"] = $penjualanMasingMasing;    
        if($data["produk"] != null) {

            $data["prediksi"] = $this->prediksi($bulan, $tahun);
            $data["label_grafik"] = $this->labelGrafik();
            $data["value_grafik"] = $this->data();
        }

        return view('dashboard', $data);
    }

    public function labelGrafik() {
        $penjualan = $this->penjualan->findAll();
        $labelMentah = [];
        foreach ($penjualan as $p) {
            $labelItem = $p["bulan"] ."/". $p["tahun"];
            array_push($labelMentah, $labelItem);
        }
        return $labelMentah;

    }

    public function prediksi($bulan, $tahun) {
        $y = $this->data(); //Y
        // dd($y);
        $x = $this->dataTengah($y); //X
        $produkTerjual = $this->total($y); //Sigma Y
        $a = $produkTerjual/count($y); //a
        $xy = $this->xy($x, $y); //XY
        $totalXy = $this->total($xy); //Sigma XY
        $x2 = $this->x2($x); //X^2
        $totalX2 = $this->total($x2); //Sigma X^2
        $b = $totalXy/$totalX2; //b
        $xBaru = $this->xBaru($bulan, $tahun, $x);
        $yBaru = floor($a+$b*$xBaru);
        return $yBaru;
    }

    // Y
    public function data () {
        $dataMentah = $this->penjualan->orderBy("tahun", "asc")->orderBy("bulan", "asc")->findAll();
        $tahunTerkecil = $this->penjualan->select("tahun")->orderBy("tahun", "asc")->first()["tahun"];
        $bulanTerkecilDariTahunTerkecil = $this->penjualan->select("bulan")->where("tahun", $tahunTerkecil)->orderBy("bulan", "asc")->first()["bulan"];
        $tahunTerbesar = $this->penjualan->select("tahun")->orderBy("tahun", "desc")->first()["tahun"];
        $bulanTerbesarDariTahunTerbesar = $this->penjualan->select("bulan")->where("tahun", $tahunTerbesar)->orderBy("bulan", "desc")->first()["bulan"];
        
        
        $tampungData = [];
        $bulan = $bulanTerkecilDariTahunTerkecil;
        $dataItem = 0;
        for($tahun = $tahunTerkecil; $tahun <= $tahunTerbesar; $tahun++) {
            foreach($dataMentah as $d) {
                if($d["tahun"] == $tahun) {                    
                    if($d["bulan"] == $bulan) {
                        $dataItem += $d["jumlah"];
                    }
                    elseif ($d["bulan"] > $bulan) {        
                        array_push($tampungData, $dataItem);
                        $dataItem = 0;
                        $bulan++;
                        $dataItem += $d["jumlah"];
                    } else {
                        // Bug
                    }
                } else {
                    $bulan = 1;
                }
            }           
            array_push($tampungData, $dataItem);
            $dataItem = 0;        
        }
        return $tampungData;        
    }

    // X
    public function dataTengah($tampungData) {
        $x = [];
        if(count($tampungData) %2 == 0) {
            $indexTerakhir = count($tampungData)-1;
            $xs = -$indexTerakhir;
            foreach($tampungData as $t) {
                array_push($x, $xs);  
                $xs = $xs+2;              
            }
        } else {
            $indexTengah = (count($tampungData)-1)/2;
            foreach($tampungData as $key => $value) {
                $xs = $key-$indexTengah;
                array_push($x, $xs);
            }

        }
        return $x;   
    }

    // Sigma Semua
    public function total($data) {
        $jumlah = 0;
        for($i = 0; $i < count($data); $i++) {
            $jumlah += $data[$i];
        }
        return $jumlah;
    }

    // XY
    public function xy($x, $y) {
        $xy = [];
        for($i = 0; $i < count($x); $i++) {
            $xyIndex = $x[$i]*$y[$i];
            array_push($xy, $xyIndex);
        }
        return $xy;
    }

    // X^2
    public function x2($x) {
        $x2 = [];
        for($i = 0; $i < count($x); $i++) {
            $xyIndex = $x[$i]*$x[$i];
            array_push($x2, $xyIndex);
        }
        return $x2;
    }

    // X Baru
    public function xBaru ($bulan, $tahun, $x) {
        $tahunTerbesar = $this->penjualan->select("tahun")->orderBy("tahun", "desc")->first()["tahun"];
        $bulanTerbesarDariTahunTerbesar = $this->penjualan->select("bulan")->where("tahun", $tahunTerbesar)->orderBy("bulan", "desc")->first()["bulan"];

        if($tahun == $tahunTerbesar) {
            $jarak = $bulan - $bulanTerbesarDariTahunTerbesar;
        } elseif($tahun - $tahunTerbesar == 1) {
            $jarakTahunLama = 12 - $bulanTerbesarDariTahunTerbesar;
            $jarak = $jarakTahunLama + $bulan;
        } else {
            $jarakTahunLama = 12 - $bulanTerbesarDariTahunTerbesar;
            $jarakTahunSelisih = 12 * ($tahun - $tahunTerbesar-1);
            $jarak = $jarakTahunLama + $jarakTahunSelisih + $bulan;
        }
        if(count($x)%2 == 0) {
            $xBaru = $x[count($x)-1] + $jarak*2;
        } else {
            $xBaru = $x[count($x)-1] + $jarak;
        }
        return $xBaru;
    }
}
