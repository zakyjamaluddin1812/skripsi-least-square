<?php

namespace App\Controllers;

class Login extends BaseController
{
    protected $user;
    public $session;

    public function __construct()
    {
        $this->session = session();
        $userModel = model('App\Models\User');
        $this->user = $userModel->first();
        
    }
    public function index()
    {
        if($this->session->has('login')) {
            return redirect()->to(base_url("/dashboard"));
        }        
        $zaky = $this->user["name"];
        $data["user"] = $zaky;
        return view("login", $data);

    }

    public function login() {
        $username = $this->request->getPost("password");
        if($this->user["password"] == $username) {
            $this->session->set('login', true);
            return $this->response->redirect(base_url("/dashboard"));
        } else {
            $this->session->setFlashdata('error', true);
            return redirect()->to(base_url('/'));
        }
    }

    public function logout() {
        $this->session->remove("login");
        return redirect()->to(base_url("/"));
    }
}
