<?php

namespace App\Controllers;

class Prediksi extends BaseController
{
    public $session;
    protected $user;
    protected $produk;
    protected $penjualan;
    protected $bulanDepan;
    protected $tahunDepan;
    protected $bulanLalu;
    protected $tahunLalu;
    public function __construct()
    {
        $this->session = session();
        $userModel = model('App\Models\User');
        $this->user = $userModel->first();
        $produk = model('App\Models\Produk');
        $this->produk = $produk;
        $penjualan = model('App\Models\Penjualan');
        $this->penjualan = $penjualan;

        $bulan = date('m');
        $tahun = date('Y');
        if ($bulan == 12) {
            $this->bulanDepan = 1;
            $this->bulanLalu = $bulan - 1;

            $this->tahunLalu = $tahun;
            $this->tahunDepan = $tahun + 1;
        } elseif ($bulan == 1) {
            $this->bulanDepan = $bulan + 1;
            $this->bulanLalu = 12;

            $this->tahunLalu = $tahun - 1;
            $this->tahunDepan = $tahun;
        } else {
            $this->bulanDepan = $bulan + 1;
            $this->bulanLalu = $bulan - 1;

            $this->tahunLalu = $tahun;
            $this->tahunDepan = $tahun;
        }
    }
    public function index()
    {
        if (!$this->session->has('login')) {
            return redirect()->to(base_url("/"));
        }
        $data["user"] = $this->user;
        $produk = $this->produk->findAll();
        $data["produk"] = $produk;
        
        $data["prediksi"] = $this->prediksi($this->bulanDepan, $this->tahunDepan);

        $prediksiSpesifik = [];
        foreach ($produk as $p) {
            if($this->penjualan->where('produk_id', $p['id'])->findAll() == null) {
                $prediksiSpesifikItem = 0;
                array_push($prediksiSpesifik, $prediksiSpesifikItem);
            } else {
                $prediksiSpesifikItem = $this->prediksiSpesifik($p["id"], $this->bulanDepan, $this->tahunDepan);
                array_push($prediksiSpesifik, $prediksiSpesifikItem);
            }
        }
        $data["prediksiSpesifik"] = $prediksiSpesifik;
        return view('prediksi', $data);
    }

    public function cari($id)
    {
        if (!$this->session->has('login')) {
            return redirect()->to(base_url("/"));
        }
        $dataPenjualan = $this->penjualan->where("produk_id", $id)->findAll();
        $data["user"] = $this->user;
        $data["produk"] = $this->produk->find($id)["name"];
        $data["id"] = $id;
        $data["bulan"] = $this->bulanDepan;
        $data["tahun"] = $this->tahunDepan;
        $data["label_grafik"] = $this->labelGrafik($id);
        $data["value_grafik"] = $this->data($dataPenjualan);
        $data["prediksiSpesifik"] = $this->prediksiSpesifik($id, $this->bulanDepan, $this->tahunDepan);
        return view('prediksi_spesifik', $data);
    }
    public function cariSemua()
    {
        if (!$this->session->has('login')) {
            return redirect()->to(base_url("/"));
        }
        $data["user"] = $this->user;
        $data["produk"] = "Semua Produk";
        $data["id"] = null;
        $data["bulan"] = $this->bulanDepan;
        $data["tahun"] = $this->tahunDepan;
        $data["prediksiSpesifik"] = $this->prediksi($this->bulanDepan, $this->tahunDepan);
        return view('prediksi_spesifik', $data);
    }

    public function prediksiSemua()
    {

        if (!$this->session->has('login')) {
            return redirect()->to(base_url("/"));
        }
        $bulan = $this->request->getPost("bulan");
        if($bulan == null || $bulan == "") {
            $bulan = $this->bulanDepan;
        }
        $tahun = $this->request->getPost("tahun");
        if($tahun == null || $tahun == "") {
            $tahun = $this->tahunDepan;
        }
        $data["user"] = $this->user;
        $data["produk"] = "Semua Produk";
        $data["id"] = null;
        $data["bulan"] = $bulan;
        $data["tahun"] = $tahun;
        $data["prediksiSpesifik"] = $this->prediksi($bulan, $tahun);
        return view('prediksi_spesifik', $data);
    }
    public function cariSpesifik($id)
    {

        if (!$this->session->has('login')) {
            return redirect()->to(base_url("/"));
        }
        $bulan = $this->request->getPost("bulan");
        if($bulan == null || $bulan == "") {
            $bulan = $this->bulanDepan;
        }
        $tahun = $this->request->getPost("tahun");
        if($tahun == null || $tahun == "") {
            $tahun = $this->tahunDepan;
        }
        $dataPenjualan = $this->penjualan->where("produk_id", $id)->findAll();
        $data["user"] = $this->user;
        $data["produk"] = $this->produk->find($id)["name"];
        $data["id"] = $id;
        $data["bulan"] = $bulan;
        $data["tahun"] = $tahun;
        $data["label_grafik"] = $this->labelGrafik($id);
        $data["value_grafik"] = $this->data($dataPenjualan);
        $data["prediksiSpesifik"] = $this->prediksiSpesifik($id, $bulan, $tahun);
        return view('prediksi_spesifik', $data);
    }



    public function labelGrafik($id)
    {
        $penjualan = $this->penjualan->where('id', $id)->findAll();
        $labelMentah = [];
        foreach ($penjualan as $p) {
            $labelItem = $p["bulan"] . "/" . $p["tahun"];
            array_push($labelMentah, $labelItem);
        }
        return $labelMentah;
    }
    public function prediksiSpesifik($id, $bulan, $tahun)
    {
        $dataMentah = $this->penjualan->where('produk_id', $id)->findAll();
        $y = $this->data($dataMentah); //Y
        $x = $this->dataTengah($y); //X
        $produkTerjual = $this->total($y); //Sigma Y
        $a = $produkTerjual / count($y); //a
        $xy = $this->xy($x, $y); //XY
        $totalXy = $this->total($xy); //Sigma XY
        $x2 = $this->x2($x); //X^2
        $totalX2 = $this->total($x2); //Sigma X^2
        $b = $totalXy / $totalX2; //b
        $xBaru = $this->xBaru($bulan, $tahun, $x);
        $yBaru = floor($a + $b * $xBaru);
        return $yBaru;
    }
    public function prediksi($bulan, $tahun)
    {
        $dataMentah = $this->penjualan->orderBy('tahun', 'asc')->orderBy('bulan', 'asc')->findAll();
        $y = $this->data($dataMentah); //Y
        // dd($y);

        $x = $this->dataTengah($y); //X
        $produkTerjual = $this->total($y); //Sigma Y

        $a = $produkTerjual / count($y); //a
        $xy = $this->xy($x, $y); //XY
        $totalXy = $this->total($xy); //Sigma XY
        $x2 = $this->x2($x); //X^2
        $totalX2 = $this->total($x2); //Sigma X^2
        $b = $totalXy / $totalX2; //b
        $xBaru = $this->xBaru($bulan, $tahun, $x);
        $yBaru = floor($a + $b * $xBaru);
        return $yBaru;
    }




    // Y
    public function data($dataMentah)
    {
        $tahunTerkecil = $this->penjualan->select("tahun")->orderBy("tahun", "asc")->first()["tahun"];
        $bulanTerkecilDariTahunTerkecil = $this->penjualan->select("bulan")->where("tahun", $tahunTerkecil)->orderBy("bulan", "asc")->first()["bulan"];
        $tahunTerbesar = $this->penjualan->select("tahun")->orderBy("tahun", "desc")->first()["tahun"];
        $bulanTerbesarDariTahunTerbesar = $this->penjualan->select("bulan")->where("tahun", $tahunTerbesar)->orderBy("bulan", "desc")->first()["bulan"];

        $tampungData = [];
        $bulan = $bulanTerkecilDariTahunTerkecil;
        $dataItem = 0;
        // dd($dataMentah);
        for ($tahun = $tahunTerkecil; $tahun <= $tahunTerbesar; $tahun++) {
            foreach ($dataMentah as $d) {
                if ($d["tahun"] == $tahun) {

                    if ($d["bulan"] == $bulan) {

                        $dataItem += $d["jumlah"];

                    } elseif ($d["bulan"] > $bulan) {
                        array_push($tampungData, $dataItem);
                        $dataItem = 0;
                        $bulan++;
                        $dataItem += $d["jumlah"];
                    } else {
                        // Bug
                    }
                } else {
                    $bulan = 1;
                }
            }
            array_push($tampungData, $dataItem);
            $dataItem = 0;
        }

        // dd($tahunTerkecil, $bulanTerkecilDariTahunTerkecil, $tahunTerbesar, $bulanTerbesarDariTahunTerbesar, $tampungData);
        return $tampungData;
    }

    // X
    public function dataTengah($tampungData)
    {
        $x = [];
        if (count($tampungData) % 2 == 0) {
            $indexTerakhir = count($tampungData) - 1;
            $xs = -$indexTerakhir;
            foreach ($tampungData as $t) {
                array_push($x, $xs);
                $xs = $xs + 2;
            }
        } else {
            $indexTengah = (count($tampungData) - 1) / 2;
            foreach ($tampungData as $key => $value) {
                $xs = $key - $indexTengah;
                array_push($x, $xs);
            }
        }
        return $x;
    }

    // Sigma Semua
    public function total($data)
    {
        $jumlah = 0;
        for ($i = 0; $i < count($data); $i++) {
            $jumlah += $data[$i];
        }
        return $jumlah;
    }

    // XY
    public function xy($x, $y)
    {
        $xy = [];
        for ($i = 0; $i < count($x); $i++) {
            $xyIndex = $x[$i] * $y[$i];
            array_push($xy, $xyIndex);
        }
        return $xy;
    }

    // X^2
    public function x2($x)
    {
        $x2 = [];
        for ($i = 0; $i < count($x); $i++) {
            $xyIndex = $x[$i] * $x[$i];
            array_push($x2, $xyIndex);
        }
        return $x2;
    }

    // X Baru
    public function xBaru($bulan, $tahun, $x)
    {
        $tahunTerbesar = $this->penjualan->select("tahun")->orderBy("tahun", "desc")->first()["tahun"];
        $bulanTerbesarDariTahunTerbesar = $this->penjualan->select("bulan")->where("tahun", $tahunTerbesar)->orderBy("bulan", "desc")->first()["bulan"];

        if ($tahun == $tahunTerbesar) {
            $jarak = $bulan - $bulanTerbesarDariTahunTerbesar;
        } elseif ($tahun - $tahunTerbesar == 1) {
            $jarakTahunLama = 12 - $bulanTerbesarDariTahunTerbesar;
            $jarak = $jarakTahunLama + $bulan;
        } else {
            $jarakTahunLama = 12 - $bulanTerbesarDariTahunTerbesar;
            $jarakTahunSelisih = 12 * ($tahun - $tahunTerbesar - 1);
            $jarak = $jarakTahunLama + $jarakTahunSelisih + $bulan;
        }
        if (count($x) % 2 == 0) {
            $xBaru = $x[count($x) - 1] + $jarak * 2;
        } else {
            $xBaru = $x[count($x) - 1] + $jarak;
        }
        return $xBaru;
    }
}
