<?php

namespace App\Controllers;

class Penjualan extends BaseController
{
    public $session;
    protected $user;
    protected $produk;
    protected $penjualan;
    public function __construct()
    {
        $this->session = session();
        $userModel = model('App\Models\User');
        $this->user = $userModel->first();
        $produk = model('App\Models\Produk');
        $this->produk = $produk;
        $penjualan = model('App\Models\Penjualan');
        $this->penjualan = $penjualan;
        
    }
    public function index()
    {
        if(!$this->session->has('login')) {
            return redirect()->to(base_url("/"));
        }
        $data["user"] = $this->user;
        $data["penjualan"] = $this->penjualan->findAll();
        $data["bulan"] = date('m');
        $data["tahun"] = date('Y');
        $data["produk"] = $this->produk->findAll();
        $data["produkSpesifik"] = null;
        foreach($this->penjualan->findAll() as $p) {
            $this->tambahOtomatis($p["id"]);
        }
        return view('penjualan_home', $data);

    }
    public function cari() {
        if(!$this->session->has('login')) {
            return redirect()->to(base_url("/"));
        }
        $produk_id = $this->request->getPost("produk_id");
        $data["penjualan"] =  $this->penjualan->where("produk_id", $produk_id)->orderBy("bulan", SORT_ASC)->findAll();
        $data["bulan"] = date('m');
        $data["tahun"] = date('Y');
        $data["produk"] = $this->produk->findAll();
        $data["produkSpesifik"] = $this->produk->find($produk_id);
        return view('penjualan', $data);
    }
    public function tambah($id) {
        if(!$this->session->has('login')) {
            return redirect()->to(base_url("/"));
        }
        // $bulan = date('m');
        // $tahun = date('Y');
        $jumlah = $this->request->getPost("jumlah_terjual");
        $bulan = $this->request->getPost("bulan");
        $tahun = $this->request->getPost("tahun");
        if($jumlah == null || $jumlah == "") {
            $jumlah = "0";
        }
        $tambah = [
            "produk_id" => $id,
            "bulan" => $bulan,
            "tahun" => $tahun,
            "jumlah" => $jumlah,
            "created_at" => date("l, d F Y")
        ];
        $this->penjualan->insert($tambah);
       
        return redirect()->back();
    }

    public function detail ($id) {
        if(!$this->session->has('login')) {
            return redirect()->to(base_url("/"));
        }
        $data["user"] = $this->user;
        $data["penjualan"] =  $this->penjualan->where("produk_id", $id)->findAll();
        $data["bulan"] = date('m');
        $data["tahun"] = date('Y');
        $data["produk"] = $this->produk->findAll();
        $data["produkSpesifik"] = $this->produk->find($id);
        return view('penjualan', $data);

    }
    public function ubah () {
        $id = $this->request->getPost("id");
        $jumlah = $this->request->getPost("jumlah_terjual");
        $bulan = $this->request->getPost("bulan");
        $tahun = $this->request->getPost("tahun");
        if($jumlah == null || $jumlah == "") {
            return redirect()->back();
        }
        
        $this->penjualan->update($id, [
            "bulan" => $bulan,
            "tahun" => $tahun,
            "jumlah" => $jumlah,
            "updated_at" => date("l, d F Y")
        ]);
        return redirect()->back();
    }

    public function hapus ($id) {
        $this->penjualan->delete($id);
        return redirect()->back();
    }

    public function tambahOtomatis($id) {
        $penjualan = $this->penjualan->findAll();
        $bulan = date('m');
        $tahun = date('Y');
        $bulanTerakhir = $penjualan[count($penjualan)-1]["bulan"];
        $tahunTerakhir = $penjualan[count($penjualan)-1]["tahun"];
        if($bulanTerakhir == $bulan-2) {
            $tambah = [
                "produk_id" => $id,
                "bulan" => $bulan-1,
                "tahun" => $tahun,
                "jumlah" => 0,
                "created_at" => date("l, d F Y")
            ];
            $this->penjualan->insert($tambah);
        }
        if($bulanTerakhir == 11 && $bulan == 1) {
            $tambah = [
                "produk_id" => $id,
                "bulan" => 12,
                "tahun" => $tahun-1,
                "jumlah" => 0,
                "created_at" => date("l, d F Y")
            ];
            $this->penjualan->insert($tambah);
        }
        if($bulanTerakhir == 12 && $bulan == 2) {
            $tambah = [
                "produk_id" => $id,
                "bulan" => 1,
                "tahun" => $tahun,
                "jumlah" => 0,
                "created_at" => date("l, d F Y")
            ];
            $this->penjualan->insert($tambah);
        }


    }

}
