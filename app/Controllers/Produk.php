<?php

namespace App\Controllers;

class Produk extends BaseController
{
    public $session;
    protected $user;
    protected $produk;
    public function __construct()
    {
        $this->session = session();
       
        $userModel = model('App\Models\User');
        $this->user = $userModel;
        $userProduk = model('App\Models\Produk');
        $this->produk = $userProduk;
        
    }
    public function index()
    {
        if(!$this->session->has('login')) {
            return redirect()->to(base_url("/"));
        }
        $data["user"] = $this->user->first();
        $data["produk"] = $this->produk->findAll();
        return view('produk', $data);
    }
    public function tambah() {
        if(!$this->session->has('login')) {
            return redirect()->to(base_url("/"));
        }
        $nama = $this->request->getPost("produk");
        if($nama == null || $nama == "") {
            return $this->response->redirect(base_url("/produk"));
        }
        $produkSama = $this->produk->where('name', $nama)->first();
        if($produkSama !== null) {
            $this->session->setFlashData('error', true);
            return redirect()->to(base_url("/produk"));
        }
        $time = date("l, d F Y");
        $produk = [
            "name" => $nama,
            "created_at" =>$time
        ];
        $this->produk->insert($produk);
        return $this->response->redirect(base_url("/produk"));

    }
    public function ubah() {
        if(!$this->session->has('login')) {
            return redirect()->to(base_url("/"));
        }
        $id = $this->request->getPost("id");
        $nama = $this->request->getPost("produk");
        if($nama == null || $nama == "") {
            return $this->response->redirect(base_url("/produk"));
        }
        $time = date("l, d F Y");
        
        $this->produk->update($id, [
            "name" => $nama,
            "updated_at" =>$time
        ]);
        return $this->response->redirect(base_url("/produk"));      

    }
    public function hapus() {
        if(!$this->session->has('login')) {
            return redirect()->to(base_url("/"));
        }
        $id = $this->request->getPost("id");
        $this->produk->delete($id);
        return $this->response->redirect(base_url("/produk"));   
    }
}
