<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Prediksi</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?=base_url("plugins/fontawesome-free/css/all.min.css")?>">
  <!-- IonIcons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url("dist/css/adminlte.min.css")?>">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      
      
      <li class="nav-item">
        <p class="nav-link" data-toggle="modal" data-target="#logout" role="button">
          <i class="fas fa-power-off"></i>
        </p>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <img src="<?=base_url("dist/img/AdminLTELogo.png")?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><?=$user["name"]?></span>
      
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/dashboard" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="/produk" class="nav-link">
              <i class="nav-icon fas fa-mobile"></i>
              <p>
                Produk
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="/penjualan" class="nav-link">
              <i class="nav-icon fas fa-shopping-cart"></i>

              <p>
                Penjualan
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="/prediksi" class="nav-link active">
              <i class="nav-icon fas fa-brain"></i>
              <p>
                Prediksi
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="/setting" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Setting
              </p>
            </a>
            
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">

                    <div class="row mb-4">
                        <div class="col-sm-6">
                            <h1 class="m-0">Prediksi Penjualan <?= $produk?></h1>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                    
                    <?php if($prediksiSpesifik != null):?>
                      <div class="col-md-3 col-4">
                        <!-- small card -->
                        <div class="small-box bg-primary">
                            <div class="inner">

                            <h3 id="perangkat">
                            <?=$prediksiSpesifik?> Terjual
                            </h3>
                
                            <p>Prediksi pada bulan <?=$bulan?> tahun <?=$tahun?></p>
                          </div>
                          <div class="icon">
                            <i class="fas fa-shopping-cart"></i>
                          </div>
                          <p data-toggle="modal" data-target="#lihatPrediksi" class="small-box-footer">
                            Lihat di bulan lain <i class="fas fa-arrow-circle-right"></i>
                          </p>
                        </div>
                      </div>
                    <?php else : ?>
                      <div class="row mb-2">
                        <div class="col-sm-6">
                            <button class="btn btn-success" data-toggle="modal" data-target="#lihatPrediksi">Lihat di bulan lain</button>
                        </div>
                    </div><!-- /.row -->
                    <?php endif ?>
                    
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->



            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                <div class="row">
          <div class="col-lg-12">
            
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
          
        </div>


                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<div class="modal fade" id="lihatPrediksi" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Lihat Prediksi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php if ($id == null) :?>
      <form action="/prediksi/semua" method="post">
        <?php else : ?>
          <form action="/prediksi/<?= $id ?>" method="post">
      <?php endif ?>
      <div class="modal-body">        
        <div class="input-group mb-3">
          <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" placeholder="Bulan" name="bulan">
          <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" placeholder="Tahun" name="tahun">
        </div>       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Lihat</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Logout-->
<div class="modal fade" id="logout" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Keluar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">        
        <div class="input-group mb-3">
          <p>Apakah anda yakin ingin keluar?</p>
        </div>       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <a href="/logout"><button type="submit" class="btn btn-danger">Keluar</button></a>
      </div>
    </div>
  </div>
</div>


<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="<?=base_url("plugins/jquery/jquery.min.js")?>"></script>
<!-- Bootstrap -->
<script src="<?=base_url("plugins/bootstrap/js/bootstrap.bundle.min.js")?>"></script>
<!-- AdminLTE -->
<script src="<?=base_url("dist/js/adminlte.js")?>"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="<?=base_url("plugins/chart.js/Chart.min.js")?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url("dist/js/demo.js")?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=base_url("dist/js/pages/dashboard3.js")?>"></script>


</body>
</html>
