<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Penjualan</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?=base_url("plugins/fontawesome-free/css/all.min.css")?>">
  <!-- IonIcons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url("dist/css/adminlte.min.css")?>">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      
      
      <li class="nav-item">
        <p class="nav-link" data-toggle="modal" data-target="#logout" role="button">
          <i class="fas fa-power-off"></i>
        </p>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <img src="<?=base_url("dist/img/AdminLTELogo.png")?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><?=$user["name"]?></span>
      
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      

      

      <!-- Sidebar Menu -->
      <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/dashboard" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="/produk" class="nav-link">
              <i class="nav-icon fas fa-mobile"></i>
              <p>
                Produk
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="/penjualan" class="nav-link active">
              <i class="nav-icon fas fa-shopping-cart"></i>

              <p>
                Penjualan
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="/prediksi" class="nav-link">
              <i class="nav-icon fas fa-brain"></i>
              <p>
                Prediksi
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="/setting" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Setting
              </p>
            </a>
            
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">

        <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Daftar Penjualan <?php echo $produkSpesifik == null ? "" : $produkSpesifik["name"]?></h1>
              
            </div><!-- /.col -->
          </div><!-- /.row -->
          
          </div>
          <div class="row">
              
              <div class="col-12">
                <div class="card">
                  
                  <!-- /.card-header -->
                  <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Bulan</th>
                          <th>Tahun</th>
                          <th>Jumlah Terjual</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i = 1?>
                        <?php foreach($penjualan as $p):?>
                        <tr>
                          <td><?= $i?></td>
                          <td><?= $p["bulan"]?></td>
                          <td><?= $p["tahun"]?></td>
                          <td><?= $p["jumlah"]?> Produk</td>
                          <td>
                          <button type="button" class="btn-sm btn btn-outline-warning" data-toggle="modal" data-target="#ubahPenjualan<?=$p["bulan"]?>"><i class="fas fa-edit"></i> UBAH</button>
                          <button type="button" class="btn-sm btn btn-outline-danger" data-toggle="modal" data-target="#hapusPenjualan<?=$p["bulan"]?>"><i class="fas fa-edit"></i> Hapus</button>
                          </td>
                        </tr>
                        <?php $i++?>
                        <?php endforeach?>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->
                <button class="btn btn-outline-success" data-toggle="modal" data-target="#penjualan">Tambah Data Penjualan</button>

              </div>
            </div>
        
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        
        
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

</div>
<!-- ./wrapper -->


<?php foreach($penjualan as $p):?>
<div class="modal fade" id="ubahPenjualan<?=$p["bulan"]?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Penjualan di bulan <?=$p["bulan"]?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/penjualan/ubah" method="post">
        <div class="modal-body">        
            <input type="hidden" name="id" value="<?=$p["id"]?>">
            <div class="form-group mb-3">
            <select required name="bulan" class="form-control" aria-label="Default select example">
                <option  value="" >Pilih bulan</option>
                <option <?= $p["bulan"] == 1 ? "selected" : ""?> value="1">Januari</option>
                <option <?= $p["bulan"] == 2 ? "selected" : ""?> value="2">Pebruari</option>
                <option <?= $p["bulan"] == 3 ? "selected" : ""?> value="3">Maret</option>
                <option <?= $p["bulan"] == 4 ? "selected" : ""?> value="4">April</option>
                <option <?= $p["bulan"] == 5 ? "selected" : ""?> value="5">Mei</option>
                <option <?= $p["bulan"] == 6 ? "selected" : ""?> value="6">Juni</option>
                <option <?= $p["bulan"] == 7 ? "selected" : ""?> value="7">Juli</option>
                <option <?= $p["bulan"] == 8 ? "selected" : ""?> value="8">Agustus</option>
                <option <?= $p["bulan"] == 9 ? "selected" : ""?> value="9">September</option>
                <option <?= $p["bulan"] == 10 ? "selected" : ""?> value="10">Oktober</option>
                <option <?= $p["bulan"] == 11 ? "selected" : ""?> value="11">Nopember</option>
                <option <?= $p["bulan"] == 12 ? "selected" : ""?> value="12">Desember</option>
              </select>
          </div>
          <div class="input-group mb-3">
            <input type="number" required min="2019" max="2050" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" placeholder="Tahun" name="tahun" value="<?=$p["tahun"]?>">
          </div>
          <div class="input-group mb-3">
            <input value="<?=$p["jumlah"]?>" type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" placeholder="Jumlah terjual" name="jumlah_terjual">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-warning">Ubah</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach?>
<?php foreach($penjualan as $p):?>
<div class="modal fade" id="hapusPenjualan<?=$p["bulan"]?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus penjualan di bulan <?=$p["bulan"]?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">        
            <p>Apakah anda yakin?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <a href="/penjualan/hapus/<?= $p['id']?>" type="submit" class="btn btn-danger">Hapus</a>
        </div>
    </div>
  </div>
</div>
<?php endforeach?>

<!-- Modalll -->
<div class="modal fade" id="penjualan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">Tambah data penjualan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="/penjualan/<?=$produkSpesifik["id"]?>/tambah" method="post">
        <div class="modal-body">        
          <div class="form-group mb-3">
            <select required name="bulan" class="form-control" aria-label="Default select example">
                <option value="" selected>Pilih bulan</option>
                <option value="1">Januari</option>
                <option value="2">Pebruari</option>
                <option value="3">Maret</option>
                <option value="4">April</option>
                <option value="5">Mei</option>
                <option value="6">Juni</option>
                <option value="7">Juli</option>
                <option value="8">Agustus</option>
                <option value="9">September</option>
                <option value="10">Oktober</option>
                <option value="11">Nopember</option>
                <option value="12">Desember</option>
              </select>
          </div>
          <div class="input-group mb-3">
          <select required name="tahun" class="form-control" aria-label="Default select example">
                <option value="" selected>Pilih tahun</option>
                <option value="2020">2020</option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
                <option value="2023">2023</option>
                <option value="2024">2024</option>
              </select>
          
          </div>
          <div class="input-group mb-3">
            <input type="hidden" name="produk_id" value="<?=$produkSpesifik==null?"":$produkSpesifik["id"]?>">
            <input autocomplete="off" type="number" required class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" placeholder="Jumlah terjual" name="jumlah_terjual">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success">Tambah</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  
<!-- Modalll Penjualan -->

<!-- Modal Logout-->
<div class="modal fade" id="logout" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Keluar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">        
        <div class="input-group mb-3">
          <p>Apakah anda yakin ingin keluar?</p>
        </div>       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <a href="/logout"><button type="submit" class="btn btn-danger">Keluar</button></a>
      </div>
    </div>
  </div>
</div>



<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="<?=base_url("plugins/jquery/jquery.min.js")?>"></script>
<!-- Bootstrap -->
<script src="<?=base_url("plugins/bootstrap/js/bootstrap.bundle.min.js")?>"></script>
<!-- AdminLTE -->
<script src="<?=base_url("dist/js/adminlte.js")?>"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="<?=base_url("plugins/chart.js/Chart.min.js")?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url("dist/js/demo.js")?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=base_url("dist/js/pages/dashboard3.js")?>"></script>
</body>
</html>
