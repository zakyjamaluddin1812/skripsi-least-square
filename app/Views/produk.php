<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Our Product</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- IonIcons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      
      
      <li class="nav-item">
        <p class="nav-link" data-toggle="modal" data-target="#logout" role="button">
          <i class="fas fa-power-off"></i>
        </p>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><?=$user["name"]?></span>
      
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      

      

      <!-- Sidebar Menu -->
      <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/dashboard" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="/produk" class="nav-link active">
              <i class="nav-icon fas fa-mobile"></i>
              <p>
                Produk
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="/penjualan" class="nav-link">
              <i class="nav-icon fas fa-shopping-cart"></i>

              <p>
                Penjualan
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="/prediksi" class="nav-link">
              <i class="nav-icon fas fa-brain"></i>
              <p>
                Prediksi
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="/setting" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Setting
              </p>
            </a>
            
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Daftar Produk</i></h1>
            
            <button class="btn btn-primary my-2" data-toggle="modal" data-target="#tambahProduk">Tambah Produk</button>
           
            


          </div><!-- /.col -->
        </div><!-- /.row -->

        <div class="row">
            <div class="col-12">
          <?php if(session()->getFlashData('error') == true) :?>

            <div class="alert alert-warning alert-dismissible fade show" role="alert">
              Waduh, Merek yang anda pilih sudah ada.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <?php endif ?>
              <div class="card">
                
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                  <table class="table table-hover text-nowrap">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Nama Produk</th>
                        <th>Ditambahkan Pada</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i = 1?>
                      <?php foreach($produk as $p) :?>
                      <tr>
                        <td><?=$i?></td>
                        <td><?=$p["name"]?></td>
                        <td><?=$p["created_at"]?></td>
                        <td>
                            <button type="button" class="btn-sm btn btn-outline-danger" data-toggle="modal" data-target="#hapus<?=$p["id"]?>"><i class="fas fa-trash"></i> HAPUS</button>
                            <button type="button" class="btn-sm btn btn-outline-warning" data-toggle="modal" data-target="#ubah<?=$p["id"]?>"><i class="fas fa-edit"></i> UBAH</button>
                        </td>
                      </tr>
                      <?php $i++?>
                      <?php endforeach ?> 
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
          </div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        
        
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

</div>
<!-- ./wrapper -->


<!-- Modal dong bro -->

<!-- Modal -->
<?php foreach($produk as $p) : ?>
<div class="modal fade" id="ubah<?=$p["id"]?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ubah Produk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/produk/ubah" method="post">
      <div class="modal-body">        
        <div class="input-group mb-3">
          <input type="hidden" name="id" value="<?=$p["id"]?>">
          <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" placeholder="Nama Produk" name="produk" value="<?=$p["name"]?>">
        </div>       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-warning">Ubah</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach?>
<!-- Modal hapus -->
<?php foreach($produk as $p) : ?>
  <div class="modal fade" id="hapus<?=$p["id"]?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Produk</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="/produk/hapus" method="post">
        <div class="modal-body">        
          <div class="input-group mb-3">
            <input type="hidden" name="id" value="<?=$p["id"]?>">
            <p>Apakah anda yakin?</p>
          </div>       
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-danger">Hapus</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<?php endforeach?>
<!-- Modal -->
<div class="modal fade" id="tambahProduk" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Produk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/produk/tambah" method="post">
      <div class="modal-body">        
        <div class="input-group mb-3">
          
          <select name="produk" class="form-control" aria-label="Default select example">
            <option selected>name type</option>
            <option value="iPhone 7">iPhone 7</option>
            <option value="iPhone 7 Plus">iPhone 7 Plus</option>
            <option value="iPhone 8">iPhone 8</option>
            <option value="iPhone 8 Plus">iPhone 8 Plus</option>
            <option value="iPhone X">iPhone X</option>
            <option value="iPhone XR">iPhone XR</option>
            <option value="iPhone XS">iPhone XS</option>
            <option value="iPhone XS MAX">iPhone XS MAX</option>
            <option value="iPhone 11">iPhone 11</option>
            <option value="iPhone 11 Pro">iPhone 11 Pro</option>
            <option value="iPhone 11 Pro MAX">iPhone 11 Pro MAX</option>
            <option value="iPhone 12 Mini">iPhone 12 Mini</option>
            <option value="iPhone 12">iPhone 12</option>
            <option value="iPhone 12 Pro">iPhone 12 Pro</option>
            <option value="iPhone 12 Pro MAX">iPhone 12 Pro MAX</option>
            <option value="iPhone 13 Mini">iPhone 13 Mini</option>
            <option value="iPhone 13">iPhone 13</option>
            <option value="iPhone 13 Pro">iPhone 13 Pro</option>
            <option value="iPhone 13 Pro MAX">iPhone 13 Pro MAX</option>
          </select>
        </div>       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success">Tambah</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Logout-->
<div class="modal fade" id="logout" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Keluar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">        
        <div class="input-group mb-3">
          <p>Apakah anda yakin ingin keluar?</p>
        </div>       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <a href="/logout"><button type="submit" class="btn btn-danger">Keluar</button></a>
      </div>
    </div>
  </div>
</div>


<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard3.js"></script>
</body>
</html>
