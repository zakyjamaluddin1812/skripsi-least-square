<?php

namespace App\Models;

use CodeIgniter\Model;

class Produk extends Model
{
    protected $table = 'produk';
    protected $allowedFields = ["name", "created_at", "updated_at"];
}