<?php

namespace App\Models;

use CodeIgniter\Model;

class Penjualan extends Model
{
    protected $table      = 'penjualan';
    protected $allowedFields = ["produk_id", "bulan", "tahun", "jumlah", "created_at", "updated_at"];
}
