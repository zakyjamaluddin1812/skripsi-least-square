<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
// $routes->get('/', 'Home::index');
$routes->get('/', 'Login::index');
$routes->post('/login', 'Login::login');
$routes->get('/dashboard', 'Dashboard::index');
$routes->get('/produk', 'Produk::index');
$routes->get('/penjualan', 'Penjualan::index');
$routes->get('/penjualan/(:num)', 'Penjualan::detail/$1');
$routes->post('/penjualan/(:num)/tambah', 'Penjualan::tambah/$1');
$routes->post('/penjualan/ubah', 'Penjualan::ubah');
$routes->post('/penjualan/hapus/(:num)', 'Penjualan::hapus');
$routes->get('/prediksi', 'Prediksi::index');
$routes->get('/prediksi/semua', 'Prediksi::cariSemua');
$routes->post('/prediksi/semua', 'Prediksi::PrediksiSemua');
$routes->get('/prediksi/(:num)', 'Prediksi::cari/$1');
$routes->post('/prediksi/(:num)', 'Prediksi::cariSpesifik/$1');
$routes->post('/produk/ubah', 'Produk::ubah');
$routes->post('/produk/hapus', 'Produk::hapus');
$routes->get('/setting', 'Setting::index');
$routes->post('/setting', 'Setting::save');
$routes->get('/logout', 'Login::logout');


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}

